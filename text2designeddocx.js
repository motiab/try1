// help https://github.com/uxal/officegen/tree/7dc4ad07c748101bac77cdd7a462ed0bdf5dca34

var async = require ( 'async' );
var officegen = require("uxal-officegen")
var fs = require('fs');
var path = require('path');

console.log('---Sample of formatted docx document - output/out.docx')

//var themeXml = fs.readFileSync ( path.resolve ( __dirname, 'input/testTheme.xml' ), 'utf8' );

var docx = officegen ( {
	type: 'docx',
	orientation: 'portrait',
    pageMargins: { top: 1000, left: 1000, bottom: 1000, right: 1000 }
    
    // author (string) - The document's author (part of the Document's Properties in Office).
    // creator (string) - Alias. The document's author (part of the Document's Properties in Office).
    // description (string) - The document's properties comments (part of the Document's Properties in Office).
    // keywords (string) - The document's keywords (part of the Document's Properties in Office).
    // orientation (string) - Either 'landscape' or 'portrait'. The default is 'portrait'.
    // pageMargins (object) - Set document page margins. The default is { top: 1800, right: 1440, bottom: 1800, left: 1440 }
    // subject (string) - The document's subject (part of the Document's Properties in Office).
    // title (string) - The document's title (part of the Document's Properties in Office).

	// The theme support is NOT working yet...
	// themeXml: themeXml
} );


// officegen.setVerboseMode ( true );
docx.on ( 'error', function ( err ) {
			console.log ( err );
		});

var pObj = docx.createP ({align: 'left'}); // align: 'left', 'right', 'center' or 'justify'
pObj.addText ( 'Simple');
pObj.addText ( 'Simple with option', {back: 'red'} ); // second parameter is options
// where options are:
// back (string) - background color code, for example: 'ffffff' (white) or '000000' (black). 
// shdType (string) - Optional pattern code to use: 'clear' (no pattern), 'pct10', 'pct12', 'pct15', 'diagCross', 'diagStripe', 'horzCross', 'horzStripe', 'nil', 'thinDiagCross', 'solid', etc.
// shdColor (string) - The front color for the pattern (used with shdType).
// bold (boolean) - true to make the text bold.
// border (string) - the border type: 'single', 'dashDotStroked', 'dashed', 'dashSmallGap', 'dotDash', 'dotDotDash', 'dotted', 'double', 'thick', etc.
// color (string) - color code, for example: 'ffffff' (white) or '000000' (black).
// italic (boolean) - true to make the text italic.
// underline (boolean) - true to add underline.
// font_face (string) - the font to use.
// font_size (number) - the font size in points.
// highlight (string) - highlight color. Either 'black', 'blue', 'cyan', 'darkBlue', 'darkCyan', 'darkGray', 'darkGreen', 'darkMagenta', 'darkRed', 'darkYellow', 'green', 'lightGray', 'magenta', 'none', 'red', 'white' or 'yellow'.

pObj.addText ( ' with color', { color: '000088' } );
pObj.addText ( ' and back color.', { color: '00ffff', back: '000088' } );

var pObj = docx.createP ();
pObj.addText ( 'Since ' );
pObj.addText ( 'officegen 0.2.12', { back: '00ffff', shdType: 'pct12', shdColor: 'ff0000' } ); // Use pattern in the background.
pObj.addText ( ' you can do ' );
pObj.addText ( 'more cool ', { highlight: true } ); // Highlight!
pObj.addText ( 'stuff!', { highlight: 'darkGreen' } ); // Different highlight color.

var pObj = docx.createP ();
pObj.addText ('Even add ');
pObj.addText ('external link', { link: 'https://github.com' });
pObj.addText ('!');

var pObj = docx.createP ();
pObj.addText ( 'Bold + underline', { bold: true, underline: true } );

var pObj = docx.createP ( { align: 'center' } );
pObj.addText ( 'Center this text', { border: 'dotted', borderSize: 12, borderColor: '88CCFF' } );

var pObj = docx.createP ();
pObj.options.align = 'right';
pObj.addText ( 'Align this text to the right.' );

var pObj = docx.createP ();
pObj.addText ( 'Those two lines are in the same paragraph,' );
pObj.addLineBreak ();
pObj.addText ( 'but they are separated by a line break.' );
docx.putPageBreak ();

var pObj = docx.createP ();
pObj.addText ( 'Fonts face only.', { font_face: 'Arial' } );
pObj.addText ( ' Fonts face and size.', { font_face: 'Arial', font_size: 40 } );
docx.putPageBreak ();

//Add an image to a paragraph:
var path = require('path');
var pObj = docx.createP ();
pObj.addImage ( path.resolve(__dirname + '/input', 'images_for_examples/image3.png' ), { cx: 300, cy: 200 } );
docx.putPageBreak ();
pObj.addImage ( path.resolve(__dirname + '/input', 'images_for_examples/image3.png' ), { cx: 100, cy: 100 } );

//To add a line break;
docx.putPageBreak ();

var pObj = docx.createP ();
pObj.addImage ( path.resolve(__dirname + '/input', 'images_for_examples/image1.png' ) );

var pObj = docx.createP ();
pObj.addImage ( path.resolve(__dirname + '/input', 'images_for_examples/sword_001.png' ) );
pObj.addImage ( path.resolve(__dirname + '/input', 'images_for_examples/sword_002.png' ) );
pObj.addImage ( path.resolve(__dirname + '/input', 'images_for_examples/sword_003.png' ) );
pObj.addText ( '... some text here ...', { font_face: 'Arial' } );
pObj.addImage ( path.resolve(__dirname + '/input', 'images_for_examples/sword_004.png' ) );

var pObj = docx.createP ();
pObj.addImage ( path.resolve(__dirname + '/input', 'images_for_examples/image1.png' ) );
docx.putPageBreak ();

var pObj = docx.createListOfNumbers ();
pObj.addText ( 'Option 1' );

var pObj = docx.createListOfNumbers ();
pObj.addText ( 'Option 2' );
pObj.addHorizontalLine ();

var pObj = docx.createP ({ backline: 'E0E0E0' });
pObj.addText ( 'Backline text1' );
pObj.addText ( ' text2' );

var table = [
	[{
		val: "No.",
		opts: {
			cellColWidth: 4261,
			b:true,
			sz: '48',
			shd: {
				fill: "7F7F7F",
				themeFill: "text1",
				"themeFillTint": "80"
			},
			fontFamily: "Avenir Book"
		}
	},{
		val: "Title1",
		opts: {
			b:true,
			color: "A00000",
			align: "right",
			shd: {
				fill: "92CDDC",
				themeFill: "text1",
				"themeFillTint": "80"
			}
		}
	},{
		val: "Title2",
		opts: {
			align: "center",
			cellColWidth: 42,
			b:true,
			sz: '48',
			shd: {
				fill: "92CDDC",
				themeFill: "text1",
				"themeFillTint": "80"
			}
		}
	}],
	[1,'All grown-ups were once children',''],
	[2,'there is no harm in putting off a piece of work until another day.',''],
	[3,'But when it is a matter of baobabs, that always means a catastrophe.',''],
	[4,'watch out for the baobabs!','END'],
]

var tableStyle = {
	tableColWidth: 4261,
	tableSize: 24,
	tableColor: "ada",
	tableAlign: "left",
	tableFontFamily: "Comic Sans MS"
}

var pObj = docx.createTable (table, tableStyle);

var out = fs.createWriteStream ( 'output/out.docx' );
out.on ( 'error', function ( err ) {
	console.log ( err );
});

async.parallel ([
	function ( done ) {
		out.on ( 'close', function () {
			console.log ( 'Finish to create a DOCX file.' );
			done ( null );
		});
		docx.generate ( out );
	}

], function ( err ) {
	if ( err ) {
		console.log ( 'error: ' + err );
	} // Endif.
});