
console.log("Server: Ari was here ----------");

try {
    var resumePdfToJson = require('linkedin-resume-pdf-to-json');
    const path = require('path');
    const fs = require('fs');

    var rootedPath = 'input/ariben-yehudaprofile.pdf';
    const absolutePath = path.join(__dirname, rootedPath);
    console.log('exists', fs.existsSync(absolutePath), absolutePath);

    var JD = "experienced deveopper from an Adtech company. understand clinets and experienced with writing requirements.";

    return resumePdfToJson(absolutePath)
        .then(function(data) {
            console.log('BEFORE: ========>',data)
            moti_debug(JSON.stringify(data))
            let adaptedString=adaptCV(JSON.stringify(data),JD);
            let ret_data = JSON.parse(adaptedString);
            console.log('AFTER: ========>', ret_data);
            return {
                'resume': ret_data,
            };
        })
        .catch(err => console.error(err));
    
}
catch(error) {
    console.error('try', error);
}


function moti_debug(data2file) {
    console.log('moti_debug');
    var fs = require("fs");
    
        // fs.writeFile(__dirname +"/output/1_ResumePdfToJson", data2file, "utf-8", function(err) {
        //     if(err) console.log(err);
        // });
        fs.open(__dirname +"/output/1_ResumePdfToJson", 'w', function(err, fd) {
            if (err) {
                throw 'error opening file: ' + err;
            }
        
            fs.write(fd, data2file, 0, data2file.length, null, function(err,writen,buf) {
                console.log(writen);
                console.log(buf);
                if (err) throw 'error writing file: ' + err;
                fs.close(fd, function(err) {
                    console.log('file written');
                })
            });
        });
}

    
    // ====================== Start algorithm ==========================
    
    function adaptCV(CV,JD) {
    
        let JD_keywords = getKeywords(JD);
        let CV_keywords = getKeywords(CV);
        let combined_keywords = combineKeywords(CV_keywords,JD_keywords);
        var i;
    
        let ret_CV = CV;
        for (i=0; i<combined_keywords.length; i++) {
            while (ret_CV.indexOf(combined_keywords[i][0])!=-1) {
                ret_CV = ret_CV.replace(combined_keywords[i][0],combined_keywords[i][1]);
            }
        }
        return ret_CV;
    }
    
    function getKeywords(str) {
    
        let keywords = [];
    
        keywords[0]=["developer","programmer"];
        keywords[1]=["design", "architecture"];
        keywords[2]=["Ad-Tech","Ad Tech","Adtech"];
        keywords[3]=["specifications", "requirements"];
        keywords[4]=["client", "customer"];
        
        let ret_kw = [];
        let ret_index = 0;
    
        var i = 0;
        var j = 0;
    
        for (i=0; i<keywords.length; i++) {
            for (j=0; j<keywords[i].length; j++) {
                if (str.indexOf(keywords[i][j]) != -1) {
                    ret_kw[ret_index] = [keywords[i][0], keywords[i][j]];
                    ret_index++;
                    break;
                }
            }
        }
    
        return ret_kw;
    }
    
    function combineKeywords(list1,list2) {
        var i;
        var j;
    
        let ret_list = [];
        let ret_index = 0;
    
        for (i=0; i<list1.length; i++) {
            for (j=0; j<list2.length; j++) {
                if (list1[i][0] == list2[j][0]) {
                    ret_list[ret_index] = [list1[i][1],list2[j][1]];
                    ret_index++;
                    break;
                }
            }
        }
    
        return ret_list;
    }
    
    // ================================= End algorithm ======================
    

