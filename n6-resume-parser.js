//const ResumeParser = require('resume-parser');
const ResumeParser = require('./resume-parser-src');


const fileDir = process.cwd() + '/input/';
ResumeParser
  .parseResume(fileDir + 'ariben-yehudaprofile.pdf', './output') //input file, output dir
  .then(file => {
    console.log("Yay! " + file);
  })
  .catch(error => {
    console.log('parseResume failed');
    console.error(error);
  });


// // From file to file
// ResumeParser
//   .parseResumeFile('./input/ariben-yehudaprofile.pdf', './output') // input file, output dir
//   .then(file => {
//     console.log("Yay! " + file);
//   })
//   .catch(error => {
//     console.error(error);
//   });

// // From URL
// ResumeParser
//   .parseResumeUrl('http://www.mysite.com/resume.txt') // url
//   .then(data => {
//     console.log('Yay! ', data);
//   })
//   .catch(error => {
//     console.error(error);
//   });