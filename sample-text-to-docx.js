
var async = require ( 'async' );
var officegen = require("uxal-officegen")
var fs = require('fs');
var path = require('path');

console.log('---Generating formated CV - output/cvout.docx')

var themeXml = fs.readFileSync ( path.resolve ( __dirname, 'input/testTheme.xml' ), 'utf8' );

var docx = officegen ( {
	type: 'docx',
	orientation: 'portrait',
	pageMargins: { top: 1800, left: 1440, bottom: 1800, right: 1440 }, //The default is { top: 1800, right: 1440, bottom: 1800, left: 1440 }
	spaceing: '1',
    author: 'AppLy',
    creator: 'Galit, Naama, Ari, Moti',
    description:'CV template from AppLy',
    keywords: 'Java, Developer, programer, web, react, javascript, product manager',
    subject: 'CV David Green - Java Expert',
    title: 'David CV title',

	// The theme support is NOT working yet...
	themeXml: themeXml
} );


//officegen.setVerboseMode ( true ); // for debug

docx.on ( 'error', function ( err ) {
			console.log ( err );
});


// Add a header:
var header = docx.getHeader ().createP ({ align: 'left' });
header.addText ( 'David Green', {bold: 'true', font_face: 'Arial', font_size: '16', color: 'black'});
header.addLineBreak (); // space after summary
header.addText ( 'Mobile:', {bold: 'true', font_face: 'Arial', font_size: 9  });
header.addText ( ' 050-1234567     ', { font_face: 'Arial', font_size: 9 });
header.addText ( 'Email:', {bold: 'true', font_face: 'Arial', font_size: 9 });
header.addText ( 'david@gmail.com', {bold: 'false', underline: 'true', color: 'blue', font_face: 'Arial', font_size: 9 });
header.addHorizontalLine ({color: 'red' });

// Please note that the object header here is a paragraph object so you can use ANY of the paragraph API methods also for header and footer.
// The getHeader () method excepting a string parameter:
// getHeader ( 'even' ) - change the header for even pages.
// getHeader ( 'first' ) - change the header for the first page only.
// to do all of that for the footer, use the getFooter instead of getHeader.
// and sorry, right now only createP is supported (so only creating a paragraph) so no tables, etc.


//Summary section
var pObj = docx.createP ({align: 'left',  backline: 'E0E0E0' }); // align: 'left', 'right', 'center' or 'justify'
pObj.addText ( 'Summary', { bold: true, underline: false, font_face: 'Arial', font_size: 11 });
// pObj.addLineBreak ();
var pObj = docx.createListOfDots ();
pObj.addText ( 'An executive level seasoned RnD Leader...bla bla bla', { font_face: 'Arial', font_size: 9 } );
var pObj = docx.createListOfDots ();
pObj.addText ( 'Can-do attitude and proven experience in managing mid to large groups bla bla', { font_face: 'Arial', font_size: 9 } );
var pObj = docx.createListOfDots ();
pObj.addText ( 'More than 10 years in Java programming bla bla...', { font_face: 'Arial', font_size: 9 } );
pObj.addLineBreak (); // space after summary

//Professional Experience
var pObj = docx.createP ({align: 'left',  backline: 'E0E0E0' });
pObj.addText ( 'Professional Experience', { bold: true, underline: false, font_face: 'Arial', font_size: 11 });
var pObj = docx.createP ();

// Period- Employer - Position
var pObj = docx.createP ({align: 'left' }); // align: 'left', 'right', 'center' or 'justify'
pObj.addText ( '2013 - 2018', 		{ bold: false, underline: false, 	font_face: 'Arial', font_size: 11 });
pObj.addText ( ' Microsoft Israel', { bold: true,  underline: true, 	font_face: 'Arial', font_size: 11 });
pObj.addText ( ' - Team Leader', 	{ bold: false, underline: false, 	font_face: 'Arial', font_size: 11 });

// var pObj = docx.createP ({align: 'left' }); // align: 'left', 'right', 'center' or 'justify'
// pObj.addLineBreak (); // space after summary
var pObj = docx.createListOfDots ({align: 'left' });
pObj.addText ( 'Software development: Lead the architecture design, development', { font_face: 'Arial', font_size: 9 });
var pObj = docx.createListOfDots ();
pObj.addText ( 'Software development: Lead the architecture design, development', { font_face: 'Arial', font_size: 9 });
var pObj = docx.createListOfDots ();
pObj.addText ( 'Software development: Lead the architecture design, development', { font_face: 'Arial', font_size: 9 });
pObj.addLineBreak (); // space after summary


// Period- Employer - Position
var pObj = docx.createP ({align: 'left' }); // align: 'left', 'right', 'center' or 'justify'
pObj.addText ( '2005 - 2012', 		{ bold: false, underline: false, 	font_face: 'Arial', font_size: 11 });
pObj.addText ( ' Google', { bold: true,  underline: true, 	font_face: 'Arial', font_size: 11 });
pObj.addText ( ' - Java Developer', 	{ bold: false, underline: false, 	font_face: 'Arial', font_size: 11 });

// var pObj = docx.createP ({align: 'left' }); // align: 'left', 'right', 'center' or 'justify'
// pObj.addLineBreak ();
var pObj = docx.createListOfDots ();
pObj.addText ( 'bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla ', { font_face: 'Arial', font_size: 9 });
var pObj = docx.createListOfDots ();
pObj.addText ( 'bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla ', { font_face: 'Arial', font_size: 9 });
var pObj = docx.createListOfDots ();
pObj.addText ( 'bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla ', { font_face: 'Arial', font_size: 9 });
pObj.addLineBreak ();

//Education
var pObj = docx.createP ({align: 'left',  backline: 'E0E0E0' });
pObj.addText ( 'Education', { bold: true, underline: false, font_face: 'Arial', font_size: 11 });
// var pObj = docx.createP ();

var pObj = docx.createListOfDots ();
pObj.addText ( 'M.Sc - Physics Science ', { font_face: 'Arial', font_size: 9 });
var pObj = docx.createListOfDots ();
pObj.addText ( 'B.Sc - Computer Science ', { font_face: 'Arial', font_size: 9 });
pObj.addLineBreak ();

//Languages
var pObj = docx.createP ({align: 'left',  backline: 'E0E0E0' });
pObj.addText ( 'Languages', { bold: true, underline: false, font_face: 'Arial', font_size: 11 });

var pObj = docx.createListOfDots ();
pObj.addText ( 'Hebrew - Mother tongue', { font_face: 'Arial', font_size: 9 });
var pObj = docx.createListOfDots ();
pObj.addText ( 'English - Fluent ', { font_face: 'Arial', font_size: 9 });
pObj.addLineBreak ();


// Footer
var footer = docx.getFooter ().createP ({ align: 'center', font_size: 9 });
// footer.addText ('Linkedin: ');
// footer.addText ('www.linkedin.com/in/moti-abudi', { link: 'www.linkedin.com/in/moti-abudi', underline: true, color: 'blue' });



// var pObj = docx.createP ( { align: 'center' } );
// pObj.addText ( 'Center this text', { border: 'dotted', borderSize: 12, borderColor: '88CCFF' } );

// var pObj = docx.createListOfNumbers ();
// pObj.addText ( 'Option 1' );
// pObj.addHorizontalLine ();


// var table = [
// 	[{
// 		val: "From-To",
// 		opts: {
// 			cellColWidth: 10,
// 			align: "left",
// 			b:true,
// 			sz: '22',
// 			fontFamily: "Arial"
// 		}
// 	},{
// 		val: "Title1",
// 		opts: {
// 			cellColWidth: 2261,
// 			align: "left",
// 			b:true,
// 		}
// 	},{
// 		val: "Title2",
// 		opts: {
// 			cellColWidth: 42,
// 			align: "left",
// 			b:true,
// 			sz: '11',
// 		}
// 	}],
// 	['2005-2007','Microsoft','Team Leader'],
// 	[2,'there is no harm in putting off a piece of work until another day.',''],
// 	[3,'But when it is a matter of baobabs, that always means a catastrophe.',''],
// 	[4,'watch out for the baobabs!','END'],
// ]

// var tableStyle = {
// 	tableColWidth: 4261,
// 	tableSize: 11,
// 	tableColor: "ada",
// 	tableAlign: "left",
// 	tableFontFamily: "Arial"
// }
// var pObj = docx.createTable (table, tableStyle);

// usage for addtext: addText ( 'text', options);
// where options is:
// back (string) - background color code, for example: 'ffffff' (white) or '000000' (black). 
// shdType (string) - Optional pattern code to use: 'clear' (no pattern), 'pct10', 'pct12', 'pct15', 'diagCross', 'diagStripe', 'horzCross', 'horzStripe', 'nil', 'thinDiagCross', 'solid', etc.
// shdColor (string) - The front color for the pattern (used with shdType).
// bold (boolean) - true to make the text bold.
// border (string) - the border type: 'single', 'dashDotStroked', 'dashed', 'dashSmallGap', 'dotDash', 'dotDotDash', 'dotted', 'double', 'thick', etc.
// color (string) - color code, for example: 'ffffff' (white) or '000000' (black).
// italic (boolean) - true to make the text italic.
// underline (boolean) - true to add underline.
// font_face (string) - the font to use.
// font_size (number) - the font size in points.
// highlight (string) - highlight color. Either 'black', 'blue', 'cyan', 'darkBlue', 'darkCyan', 'darkGray', 'darkGreen', 'darkMagenta', 'darkRed', 'darkYellow', 'green', 'lightGray', 'magenta', 'none', 'red', 'white' or 'yellow'.



var out = fs.createWriteStream ( 'output/cvout.docx' );
out.on ( 'error', function ( err ) {
	console.log ( err );
});

async.parallel ([
	function ( done ) {
		out.on ( 'close', function () {
			console.log ( 'Finish to create a DOCX file.' );
			done ( null );
		});
		docx.generate ( out );
	}

], function ( err ) {
	if ( err ) {
		console.log ( 'error: ' + err );
	} // Endif.
});