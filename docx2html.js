// convert file output.docx to html

var mammoth = require("shimo-mammoth");

var html;


mammoth.convertToHtml({path: __dirname+"/output/merged-output.docx"})
.then(function(result){
    ROOT_APP_PATH = fs.realpathSync('.'); console.log("path=" + ROOT_APP_PATH); // just print the current directory
    
    html = result.value; // The generated HTML 
    var messages = result.messages; // Any messages, such as warnings during conversion
    console.log(html);
})
.done();

// Demonstarte the output in both server and also to file
var http = require('http'); 
var fs = require("fs"); // create server to demonstarte the html output

http.createServer(function(request, response) {
    
    fs.open(__dirname +"/output/out.html", 'w', function(err, fd) {
        if (err) {
            throw 'error opening file: ' + err;
        }
    
        // save the html file
        fs.write(fd, html, 0, html.length, null, function(err,writen,buf) {
            console.log(writen);
            console.log(buf);
            if (err) throw 'error writing file: ' + err;
            fs.close(fd, function(err) {
                console.log('file written');
            })
        });
    });

    // show html page on the localhost:3000
    response.writeHead(200, {'Content-Type': 'text/html'});
    response.write(html);
    response.end();

}).listen(3000);
