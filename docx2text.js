// convert file output.docx to text without formating

var mammoth = require("shimo-mammoth");
var countWords = require("count-words")
var countWordsArray = require("count-words-array");


// mammoth.convertToHtml({path: __dirname+"/output/merged-output.docx"})
mammoth.extractRawText({path: __dirname+"/output/merged-output.docx"})
.then(function(result){

    var txt = result.value; // The raw text 
    var messages = result.messages; // Any messages, such as warnings during conversion 
 
    // print to log the contents
    console.log("--------------");
    console.log("text Length=", txt.length);

    // count each word in the text, output to console as pure text
    console.log(countWords(txt, true));

    console.log("--------------");
    
    // count each word in the text, output to console as an array
    //  => [ { name: 'Home', count: 1 },
    //       { name: 'sweet', count: 1 },
    //       { name: 'home', count: 1 } ]
    console.log(countWordsArray(txt, true));
    
    console.log("--------------");
    
})
.done();

