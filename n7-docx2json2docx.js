const ResumeParser = require('./resume-parser-src'); //Moti: I had to modify the npm package since it had syntax error

const inputFile = 'cv-in.docx';
// const inputFile = 'ariben-yehudaprofile.pdf';
// const inputFile = 'examplecv.docx';
// const inputFile = 'cvout.docx';
// const inputFile = 'resume.txt';


const inputDir = process.cwd() + '\\input\\';
const outputDir = process.cwd() + '\\output\\';

//Generate a JSON file out of the resume input file (pdf, doc, docx, )
console.log('\nStep 1 - Resume to JSON');
ResumeParser
  .parseResume(inputDir + inputFile, outputDir) //input file, output dir
  .then(file => {
    console.log("Yay! " + file);
    process_json(outputDir, inputFile+'.json');
  })
  .catch(error => {
    console.log('parseResume failed');
    console.error(error);
  });



function process_json(inputDir, inputFile) {
    console.log('\nprocess_json', outputDir, "---", inputFile);

    // Load resume JSON file
    json = require(inputDir + inputFile);

    //process json
    matched_json = analyze_json(json); // Here comes the matching algotithm
    generate_cv(matched_json);
    
    // const loadJsonFile = require('load-json-file');  
    // loadJsonFile(inputDir + inputFile).then(json => {
    //     analyze_json(json);
    //     generate_cv(json);
    //     // console.log(json);
    //     //=> {foo: true}
    //     console.log('-------------------------------');
    // });
  }

// Split sections into separate lines
function analyze_json(jsonResume) {
    console.log('analyze_json');

    cv = jsonResume;
    
    for (key in cv) {
        console.log('~~~~~', key);
        // paragraph_to_lines(cv[key]);
        var a = cv[key].split("\n");
        for (i in a) {
            console.log('--->', i, a[i]);
        }
    }
    return cv;
}

// Generate a docx cv file
function generate_cv(jsonResume) {
    console.log('generate_cv');

    cv = jsonResume;
    //console.log(cv['name'],cv['phone'], cv['email']);

    // create a blank doc object (still not a file)
    var docx = cv_init_docx(cv['name'], cv['phone'], cv['email'], 'www.linkedin.com/in/moti-abudi');
    
    // regular experession for position matching by date i.e. 19xx-20xx
    regular_date = /(^(19|20)[0-9]{2}\s*\W|\W(19|20)[0-9]{2}\W|\W(19|20)[0-9]{2}$)/;
    var pObj;
    var first_position_flag = 0;

    for (key in cv) {

        // filter out unwanted json sections
            // if (key == 'name') continue;
            // if (key == 'email') continue;
            // if (key == 'phone') continue;
            // if (key == 'objective') continue;
            // if (key == 'objective') continue;
            // 'technology', 'skills', 'Skills & Expertise','technologies', 'projects', 'interests', 'positions', 'profiles', 'awards', 'honors', 'certifications'

        if ((key == 'summary') || (key == 'experience') || (key == 'education') || (key == 'courses') || (key == 'languages') ) {

            pObj = cv_add_section_heading(docx, key);
            // paragraph_to_lines(cv[key]);
            var a = cv[key].split("\n");
            for (i in a) {
                text = a[i];
                if (regular_date.test(a[i]) == true ) { // if this is a position heading (check by looking for date 19xx or 20xx)
                    if (first_position_flag > 0) {
                        //pObj = cv_add_blank_line(docx); // space between each position 
                        pObj.addLineBreak ();     
                    }
                    pObj = cv_add_position_heading(docx, pObj, text);
                    first_position_flag ++;
                    continue;
                }
                cv_add_position_contents(pObj, text);
                // pObj = cv_add_blank_line(docx); // space between each position 
            }
            // cv_add_blank_line(docx); // space between each section
        } 
        else {
            continue; // skip un-desiered sections
        }
    }
    
    // make a file out of the docx object
    create_docx_file(docx);

    return ;
}


//
// -----------------------------------------------------------------------------------
//

var async = require ( 'async' );
var officegen = require("uxal-officegen")
var fs = require('fs');
// var path = require('path');

const FontName = 'Arial';
const FontSize_header_big           = 16;
const FontSize_header_small         = '9';
const FontSize_section_heading      = '11';
const FontSize_section_contents     = '9';
const FontSize_position_heading     = '10';
const FontSize_position_contents    = '9';
const FontSize_regular              = '9';
const indent_content                = '  -  ';
const indent_position               = ' ';


function cv_init_docx(name, phone, email, linkedin) {
    var docx = officegen ( {
            type: 'docx',
            orientation: 'portrait',
            pageMargins: { top: 1800, left: 1440, bottom: 1800, right: 1440 }, //The default is { top: 1800, right: 1440, bottom: 1800, left: 1440 }
            spaceing: '1',
            author: 'The AppLy team',
            creator: 'Galit, Naama, Ari, Moti',
            description:'CV template from AppLy',
            keywords: 'Java, Developer, programer, web, react, javascript, product manager',
            subject: 'Adapted CV by AppLy',
            title: 'AppLy (c)',

            // The theme support is NOT working yet...
            //themeXml: themeXml
        } );

    cv_add_header(docx, name, phone, email);
    cv_add_footer(docx, linkedin);
    
    return docx;
}


function cv_add_header(docx, name, cel_number, email) {
    var header = docx.getHeader ().createP ({ align: 'left' });
    if (name == null) {
        name = 'John Doe ???';
    }
    header.addText ( name,                  {font_face: FontName, font_size: FontSize_header_big, bold: 'true', color: 'black'});
    header.addLineBreak ();

    if (cel_number == null) {
        cel_number = '05x-???????'
    }
    header.addText ( 'Cel: ',               {font_face: FontName, font_size: FontSize_header_small, bold: 'true' });
    header.addText ( cel_number+'     ',    {font_face: FontName, font_size: FontSize_header_small });

    if (email == null) {
        email = '???@???????.??'
    }
    header.addText ( 'Email:',              {font_face: FontName, font_size: FontSize_header_small, bold: 'true' });
    header.addText ( email,                 {font_face: FontName, font_size: FontSize_header_small, bold: 'false', underline: 'true', color: 'blue'});

    header.addHorizontalLine ();
}


function cv_add_blank_line(docx) {
     // adds line break
     var pObj = docx.createP ({align: 'left' });
     return pObj;
}


function cv_add_section_heading(docx, heading) {

    //Summary section
    var pObj = docx.createP ({align: 'left',  backline: 'E0E0E0' }); // align: 'left', 'right', 'center' or 'justify'
    
    // Make capitals on start of heading
    var s = heading[0].toUpperCase() + heading.substr(1,heading.length);

    pObj.addText (s,                            {font_face: FontName, font_size: FontSize_section_heading, bold: true, underline: false });
    var pObj = docx.createP ({align: 'left' });
    return pObj;
}


function cv_add_section_contents(pObj, data) {
    // var pObj = docx.createListOfDots ();
    pObj.addText ( indent_content+data,                        { font_face: FontName, font_size: FontSize_section_contents } );
}


function cv_add_position_heading(docx, pObj, data) {
    // parse position heading
    regular_position = /(\d{4}\s+-\s+\d{4})\s+(.*)\s-\s(.*)/;
    var res = regular_position.exec(data);
    var at_period = res[1];
    var at_company = res[2];
    var at_position = res[3];

    //var pObj = docx.createP ({align: 'left' });
    //pObj.addLineBreak ();
    
    if ( (at_period == null) || (at_company == null) || (at_position == null) ) {
        // line format is un identified so print as is
        pObj.addText ( data, 		                {font_face: FontName, font_size: FontSize_position_heading, bold: false, underline: false });
    }
    else {
        pObj.addText ( indent_position + at_period + indent_position,  {font_face: FontName, font_size: FontSize_position_heading, bold: true, underline: false });
        pObj.addText ( at_company,                  {font_face: FontName, font_size: FontSize_position_heading, bold: false, underline: true  });
        pObj.addText ( ' - ',                       {font_face: FontName, font_size: FontSize_position_heading, bold: false, underline: false });
        pObj.addText ( at_position,                 {font_face: FontName, font_size: FontSize_position_heading, bold: true,  underline: false });
    }
    // var pObj = docx.createListOfDots ({align: 'left' });
    //var pObj = docx.createP ({align: 'left' });
    pObj.addLineBreak (); // space after summary
    return pObj;
 }
  
 
function cv_add_position_contents(pObj, data) {

    //var pObj = docx.createListOfDots ({align: 'left' });
    pObj.addText ( indent_content+data,                { font_face: FontName, font_size: FontSize_regular });
    pObj.addLineBreak (); // space after summary
    return pObj;
}
    

function cv_add_footer(docx, linkedin) {
    // Footer
    var footer = docx.getFooter ().createP ({ align: 'center', font_size: FontSize_regular });
    if (linkedin != null) {
        footer.addText ('Linkedin: ');
        // footer.addText (linkedin, { link: linkedin, underline: true, color: 'blue' }); // from some reason, this line causes a corrupted word
    }

}



function create_docx_file(docx) {
    var out = fs.createWriteStream ( outputDir + inputFile );
    out.on ( 'error', function ( err ) {
        console.log ( err );
    });

    async.parallel ([
        function ( done ) {
            out.on ( 'close', function () {
                console.log ( 'Finish to create a DOCX file.' );
                done ( null );
            });
            docx.generate ( out );
        }

    ], function ( err ) {
        if ( err ) {
            console.log ( 'error: ' + err );
        } // Endif.
    });
}