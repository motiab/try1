var builder = require('docx-builder');
var docx = new builder.Document();

//SET THE HEADER
console.log("set Header");

docx.beginHeader();
docx.insertText("This is the header");
docx.endHeader();
 
//SET THE FOOTER
 
docx.beginFooter();
docx.insertText("This is the footer");
docx.endFooter();
 
//COMMON OPERATIONS
 
docx.setBold();
docx.unsetBold();
docx.setItalic();
docx.unsetItalic();
docx.setUnderline();
docx.unsetUnderline();
docx.setFont("Arial");
docx.setSize(40);
docx.rightAlign();
docx.centerAlign();
docx.leftAlign();
docx.insertText("Moti was here");
 
//INSERT A TABLE
 
docx.beginTable({ borderColor: "red", borderSize: 20 });
docx.insertRow();
docx.setItalic();
docx.setUnderline();
docx.insertText("ID");
docx.nextColumn();
docx.insertText("Username");
docx.nextRow();
docx.insertText("1");
docx.nextColumn();
docx.insertText("raulb");
docx.endTable();

//SAVE THE DOCX FILE
console.log("Save docx");
 
docx.save(__dirname + "/output/output.docx", function(err){
    if(err) console.log(err);
});
 

//Merge EXTERNAL FILES
console.log("Mergeing start ...");
docx.insertPageBreak();
docx.insertDocxSync(__dirname+"/originalcv/test1.docx");
docx.insertPageBreak();
docx.insertDocxSync(__dirname+"/originalcv/test2.docx");
docx.insertPageBreak();

// console.log("insert docx");
// docx.insertDocx(__dirname+"/originalcv/test1.docx", function(err){
//     if(err) console.log(err);
// });
console.log("Mergeing finish ...");


//SAVE THE DOCX FILE
console.log("Save the merged files");
docx.save(__dirname + "/output/merged-output.docx", function(err){
    if(err) console.log(err);
});
 
