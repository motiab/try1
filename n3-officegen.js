var async = require ( 'async' );
var officegen = require('officegen');

var fs = require('fs');
var path = require('path');

var themeXml = fs.readFileSync ( path.resolve ( __dirname, 'input/testTheme.xml' ), 'utf8' );

var docx = officegen ( {
	type: 'docx',
	orientation: 'portrait',
	pageMargins: { top: 1000, left: 1000, bottom: 1000, right: 1000 }
	// The theme support is NOT working yet...
	// themeXml: themeXml
} );

// Remove this comment in case of debugging Officegen:
// officegen.setVerboseMode ( true );

docx.on ( 'error', function ( err ) {
			console.log ( err );
		});

var pObj = docx.createP ({align: 'left', color: 'red', indentLevel: '14', char_spacing: '15'});
//var pObj = docx.createP ({align: 'left'});

var pObj = docx.createP ();
pObj.addText ( 'Since 1' );
pObj.addLineBreak ();
pObj.addText ( 'Since 2' );
pObj.addLineBreak ();
pObj.addText ( 'Since 3' );
pObj.addLineBreak ();

var pObj = docx.createP ({align: 'left', indentLevel: '34', char_spacing: '35'});
pObj.addText ( 'Since 1' );
pObj.addLineBreak ();
pObj.addText ( 'Since 2' );
pObj.addLineBreak ();
pObj.addText ( 'Since 3' );
pObj.addLineBreak ();

var out = fs.createWriteStream ( 'output/n3-officegen.docx' );

out.on ( 'error', function ( err ) {
	console.log ( err );
});

async.parallel ([
	function ( done ) {
		out.on ( 'close', function () {
			console.log ( 'Finish to create a DOCX file.' );
			done ( null );
		});
		docx.generate ( out );
	}

], function ( err ) {
	if ( err ) {
		console.log ( 'error: ' + err );
	} // Endif.
});