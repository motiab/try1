// convert file output.docx to html

var mammoth = require("shimo-mammoth");

var html;
var txt;

mammoth.extractRawText({path: __dirname+"/output/merged-output.docx"})
.then(function(result){
    
    html = result.value; // The raw text 
    var messages = result.messages; // Any messages, such as warnings during conversion 

})
.done();

// mammoth.convertToHtml({path: __dirname+"/output/merged-output.docx"})
// .then(function(result){
//     ROOT_APP_PATH = fs.realpathSync('.'); console.log("path=" + ROOT_APP_PATH); // just print the current directory
    
//     html = result.value; // The generated HTML 
//     // var html = result.value; // The generated HTML 
//     var messages = result.messages; // Any messages, such as warnings during conversion 
// })
// .done();


//__dirname+"/originalcv/test1.docx"

var http = require('http');
var fs = require("fs");

http.createServer(function(request, response) {
    
    // fs.writeFile(__dirname +"/output/out.html", html, "utf-8", function(err){
    //     if(err) console.log(err);
    // });
    fs.open(__dirname +"/output/out.html", 'w', function(err, fd) {
        if (err) {
            throw 'error opening file: ' + err;
        }
    
        fs.write(fd, html, 0, html.length, null, function(err,writen,buf) {
            console.log(writen);
            console.log(buf);
            if (err) throw 'error writing file: ' + err;
            fs.close(fd, function(err) {
                console.log('file written');
            })
        });
    });

    response.writeHead(200, {'Content-Type': 'text/html'});
    response.write(html);
    response.end();

}).listen(3000);
